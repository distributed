//
// Copyright (C) 2008 Francesco Salvestrini
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//

#include "config.h"

#include <unistd.h>
#include <string>
#include <iostream>

#include "getopt.h"

#include "libs/misc/debug.h"
#include "libs/misc/environment.h"
#include "libs/conf/configuration.h"

#define PROGRAM_NAME "distribute"

void version(void)
{
	std::cout
		<< PROGRAM_NAME << " (" << PACKAGE_NAME  << ") " << PACKAGE_VERSION <<               std::endl
		<<                                                                                   std::endl
		<< "Copyright (C) 2008 Francesco Salvestrini" <<                                     std::endl
		<<                                                                                   std::endl
		<< "This is free software.  You may redistribute copies of it under the terms of" << std::endl
		<< "the GNU General Public License <http://www.gnu.org/licenses/gpl.html>." <<       std::endl
		<< "There is NO WARRANTY, to the extent permitted by law." <<                        std::endl;

}

#define DEFAULT_TIMEOUT 10

void help(void)
{
	std::cout
		<< "Usage: " << PROGRAM_NAME << " [OPTION]... "<<                                     std::endl
		<<                                                                                    std::endl
		<< "Options: " <<                                                                     std::endl
		<< "  -t, --time-out=TIME    time-out in secs (default " << DEFAULT_TIMEOUT << ")" << std::endl
		<< "  -j, --job=FILE         use FILE as job to distribute" <<                        std::endl
		<< "  -d, --debug            enable debugging traces" <<                              std::endl
		<< "  -h, --help             print this help, then exit" <<                           std::endl
		<< "  -V, --version          print version number, then exit" <<                      std::endl
		<<                                                                                    std::endl
		<< "Report bugs to <" << PACKAGE_BUGREPORT << ">" <<                                  std::endl;
}

void hint(const std::string & message)
{
	BUG_ON(message.size() == 0);

	std::cout
		<< message <<                                                 std::endl
		<< "Try `" << PROGRAM_NAME << " -h' for more information." << std::endl;
}

int main(int argc, char * argv[])
{
	TR_CONFIG_LVL(TR_LVL_DEFAULT);
	TR_CONFIG_PFX(PROGRAM_NAME);

	try {
		std::string conffile     = "";

		int         time_out     = DEFAULT_TIMEOUT;
		bool        time_out_set = false;
		std::string job          = "";

		int c;
		//int digit_optind = 0;
		while (1) {
			//int this_option_optind = optind ? optind : 1;
			int option_index       = 0;

			static struct option long_options[] = {
				{ "config",   1, 0, 'c' },
				{ "job",      1, 0, 'j' },
				{ "time-out", 1, 0, 't' },
				{ "debug",    0, 0, 'd' },
				{ "verbose",  0, 0, 'v' },
				{ "version",  0, 0, 'V' },
				{ "help",     0, 0, 'h' },
				{ 0,          0, 0, 0   }
			};

			c = getopt_long(argc, argv, "c:t:j:dvVh",
					long_options, &option_index);
			if (c == -1) {
				break;
			}

			switch (c) {
				case 'c':
					conffile = optarg;
					break;
				case 't':
					//
					// XXX FIXME:
					//     The current solution is ugly.
					//     Add modifiers (s, m, h) to
					//     this option in order to use
					//     different time granularity.
					//
					time_out = atoi(optarg);
					if (time_out < 0) {
						hint("Wrong time-out value");
						return 1;
					}
					time_out_set = true;
					break;
				case 'j':
					job = optarg;
					break;
				case 'd':
					TR_CONFIG_LVL(TR_LVL_DEBUG);
					break;
				case 'v':
					TR_CONFIG_LVL(TR_LVL_VERBOSE);
					break;
				case 'V':
					version();
					return 0;
				case 'h':
					help();
					return 0;
				case '?':
					hint("Unrecognized option");
					return 1;

				default:
					BUG();
					return 1;
			}
		}

		// Options related checks
		BUG_ON(time_out < 0);

		// Build configuration file path
		if (conffile.size() == 0) {
			std::string homedir = Environment::get("HOME");
			std::string confdir =
				homedir          +
				std::string("/") +
				std::string(".") +
				std::string(PACKAGE_TARNAME);
			conffile =
				confdir          +
				std::string("/") +
				std::string(PROGRAM_NAME);
		} else {
			TR_DBG("Configuration file overridden\n");
		}

		BUG_ON(conffile.size() == 0);

		TR_DBG("Initial (configuration file) values:\n");
		TR_DBG("  Time out:           '%d'\n", time_out);

		// Read configuration file (if available)
		try {
			TR_DBG("Reading configuration file from '%s'\n",
			       conffile.c_str());

			Configuration::File config;
			std::ifstream       instream(conffile.c_str());

			instream >> config;

			int conf_timeout;

			if (config.get<int>(conf_timeout, "time-out")) {
				TR_DBG("Found 'max-mem' key, value '%d'\n",
				       conf_timeout);

				// Check gathered configuration
				if (time_out < 0) {
					TR_ERR("Wrong time-out value in "
					       "configuration file");
					return 1;
				}

				if (!time_out_set) {
					TR_DBG("Updating 'time-out' key\n");

					// Configuration value not specified
					// in command line ...
					time_out = conf_timeout;
				}
			}
		} catch (std::exception & e) {
			TR_ERR("%s\n", e.what());
		} catch (...) {
			BUG();
		}

		// Options related checks
		BUG_ON(time_out < 0);

		TR_DBG("Final (configuration file) values:\n");
		TR_DBG("  Time out:           '%d'\n", time_out);

		// Dump (acquired and derived) infos
		TR_DBG("Final values:\n");
		TR_DBG("  Time out:           '%d'\n", time_out);
		TR_DBG("  Job:                '%s'\n", job.c_str());
		TR_DBG("  Configuration file: '%s'\n", conffile.c_str());

		// Assert what is needed to be asserted
		BUG_ON(time_out < 0);

		// Do the supposed work
		MISSING_CODE();
	} catch (...) {
		BUG();
	};

	return 0;
}
