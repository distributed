//
// Copyright (C) 2008 Francesco Salvestrini
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//

#include "config.h"

extern "C" {
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
}

#include "libs/misc/trace.h"
#include "libs/vm/lua.h"

using namespace VM;

Lua::Lua(size_t memory) :
	VM(memory)
{
	TR_DBG("Creating Lua virtual machine %p\n", this);
}

Lua::~Lua(void)
{
	TR_DBG("Destroying Lua virtual machine %p\n", this);

	if (state_) {
		lua_close(state_);
	}
}

bool Lua::run(void)
{
	TR_DBG("Running Lua virtual machine %p\n", this);

	return true;
}
