//
// Copyright (C) 2008 Francesco Salvestrini
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//

#ifndef LIBS_VM_LUA_H
#define LIBS_VM_LUA_H

#include "config.h"

#ifdef HAVE_LUA

extern "C" {
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
}
#include <cstdlib>

#include "libs/vm/vm.h"

namespace VM {
	class Lua : public VM {
	public:
		Lua(size_t memory);
		~Lua(void);

		bool run(void);

	protected:
		Lua(void);

	private:
		lua_State * state_;
	};
};

#endif // HAVE_LUA
#endif // LIBS_VM_LUA_H
