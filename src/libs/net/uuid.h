//
// Copyright (C) 2008 Francesco Salvestrini
//                    Gino Carrozzo
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//

#ifndef LIBS_NET_UUID_H
#define LIBS_NET_UUID_H

#include "config.h"

#include <string>
#include "libs/misc/exception.h"

namespace Net {

	class UUID {
	public:
		typedef enum {
			UNKNOWN                     = 0,
			IETF_RFC4122_TIMEBASED      = 1,
			DCE_SECURITY_POSIX_UID      = 2,
			IETF_RFC4122_NAMEBASED_MD5  = 3,
			IETF_RFC4122_RANDOM         = 4,
			IETF_RFC4122_NAMEBASED_SHA1 = 5
		} version_t;

		UUID(void);
		~UUID(void);

		UUID(const std::string & u);
		UUID(version_t version);

		void  clear(void);

		// Operators
		operator              std::string(void);
		bool                  operator ==(const UUID & rhs);
		UUID &                operator =(const UUID & rhs);
		friend std::ostream & operator <<(std::ostream & stream,
						 UUID           obj);
		friend std::istream & operator >>(std::istream & stream,
						 UUID &         obj);

		// Exceptions
		class invalid_version : public Exception {
		public:
			version_t version;

			invalid_version(version_t ver,
					const std::string & description) :
				Exception(description),
				version(ver) {};

			~invalid_version(void) throw () {};

		protected:
			invalid_version(void);
		private:
			// nothing more for the time being
		};

		class invalid_hexfield : public Exception {
		public:
			uint8_t position;

			invalid_hexfield(uint8_t pos,
					 const std::string & description) :
				Exception(description),
				position(pos)
				{};

			~invalid_hexfield(void) throw () {};

		protected:
			invalid_hexfield(void);
		private:
			// nothing more for the time being
		};

		class formatting_problem : public Exception {
		public:
			version_t version;
			int       field;

			formatting_problem(version_t ver,
					   int       f,
					   const std::string & description) :
				Exception(description),
				version(ver),
				field(f)
				{};

			~formatting_problem(void) throw () {};

		protected:
			formatting_problem(void);
		private:
			// nothing more for the time being
		};

		class scrambling_problem : public Exception {
		public:
			version_t version;

			scrambling_problem(version_t ver,
					   const std::string & description) :
				Exception(description),
				version(ver)
				{};

			~scrambling_problem(void) throw () {};

		protected:
			scrambling_problem(void);
		private:
			// nothing more for the time being
		};

		class generic_problem : public Exception {
		public:
			generic_problem(const std::string & description) :
				Exception(description)
				{};

			~generic_problem(void) throw () {};

		protected:
			generic_problem(void);
		private:
			// nothing more for the time being
		};

	protected:

	private:
//   From RFC4122
//
//   The fields are encoded as 16 octets, with the sizes and order of the
//   fields defined above, and with each field encoded with the Most
//   Significant Byte first (known as network byte order).  Note that the
//   field names, particularly for multiplexed fields, follow historical
//   practice.
//
//   0                   1                   2                   3
//    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//   |                          time_low                             |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//   |       time_mid                |         time_hi_and_version   |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//   |clk_seq_hi_res |  clk_seq_low  |         node (0-1)            |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//   |                         node (2-5)                            |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

		struct uuid {
			uint32_t time_low;
			uint16_t time_mid;
			uint16_t time_hi_and_version;
			uint16_t clock_seq;
			uint8_t  node[6];
		} uuid_;

		bool grab_4hex(const std::string &    u,
			       std::string::size_type s,
			       std::string::size_type e,
			       uint16_t &             v);
		bool grab_8hex(const std::string &    u,
			       std::string::size_type s,
			       std::string::size_type e,
			       uint32_t &             v);
		bool grab_12hex(const std::string &    u,
				std::string::size_type s,
				std::string::size_type e,
				uint8_t *              v);

		void format_uuid_tb(void);
		void format_uuid_nb(void);
		void format_uuid_random(void);
		void scramble_sha1(void);
		void scramble_md5(void);

		void check_uuid(void);
	};
};

#endif // LIBS_NET_UUID_H
