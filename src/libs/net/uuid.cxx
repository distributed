//
// Copyright (C) 2008 Francesco Salvestrini
//                    Gino Carrozzo
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//

#include "config.h"

#include <cstdlib>
#include <stdexcept>
#include <sstream>
#include <iomanip>
#include <cstring>
#include <string>
#include <iostream>

#include <stdlib.h>   // XXX-FIXME to remove with rand/srand wraps in gnulib
#include <sys/time.h> // XXX-FIXME to remove with rand/srand wraps in gnulib

#include "libs/net/uuid.h"


using namespace Net;

static uint32_t myrand(uint32_t min, uint32_t max)
{
	struct timeval tv;

	float fmax;
	float fmin;

	if (gettimeofday(&tv, NULL) == -1) {
		return 0;
	}
	srand(tv.tv_usec);

	fmax = (max != 0xFFFFFFFF) ? (float) max : (float)RAND_MAX;
	fmin = min ? (float) min : 0.0;

	return (min + (uint32_t) (fmax * (rand() / (RAND_MAX + 1.0))));
}

UUID::UUID(void)
{
	format_uuid_random();
	check_uuid();
}

UUID::~UUID(void)
{
}

UUID::UUID(version_t version)
{
	memset(&uuid_, 0, sizeof(uuid_));

	switch (version) {
		case IETF_RFC4122_TIMEBASED: {
			format_uuid_tb();
		}
			break;
		case DCE_SECURITY_POSIX_UID: {
		}
			break;
		case IETF_RFC4122_NAMEBASED_MD5: {
			format_uuid_nb();
			scramble_md5();
		}
			break;
		case IETF_RFC4122_NAMEBASED_SHA1: {
			format_uuid_nb();
			scramble_sha1();
		}
			break;
		case IETF_RFC4122_RANDOM: {
			format_uuid_random();
		}
			break;
		default: {
			throw invalid_version(version,
					      "not supported in IETF RFC4122");
		}
			break;
	}

	check_uuid();
}

//
// NOTE (excerpt from wikipedia):
//
//   In its canonical form, a UUID consists of 32 hexadecimal digits, displayed
//   in 5 groups separated by hyphens, in the form 8-4-4-4-12 for a total of
//   36 characters. For example:
//
//     550e8400-e29b-41d4-a716-446655440000
//

bool UUID::grab_8hex(const std::string &    u,
		     std::string::size_type s,
		     std::string::size_type e,
		     uint32_t &             v)
{
	if ((e - s) != 8) {
		return false;
	}

	std::string tmp;
	tmp = u.substr(s, e);

	const char * t;
	t = tmp.c_str();

	v = strtoul(t, 0, 16);

	return true;
}

bool UUID::grab_4hex(const std::string &    u,
		     std::string::size_type s,
		     std::string::size_type e,
		     uint16_t &             v)
{
	if ((e - s) != 4) {
		return false;
	}

	std::string tmp;
	tmp = u.substr(s, e);

	const char * t;
	t = tmp.c_str();

	v = strtoul(t, 0, 16);

	return true;
}

bool UUID::grab_12hex(const std::string &    u,
		      std::string::size_type s,
		      std::string::size_type e,
		      uint8_t *              v)
{
	if ((e - s) != 12) {
		return false;
	}

	std::string tmp;
	tmp = u.substr(s, e);

	int  i;
	char buf[3];

	buf[2] = 0;
	for (i = 0; i < 6; i++) {
		buf[0] = tmp[i * 2];
		buf[1] = tmp[i * 2 + 1];
		v[i] = strtoul(buf, 0, 16);
	}

	return true;
}

UUID::UUID(const std::string & u)
{
	std::string::size_type s, e;

	s = 0;

	// Grab the time-low part
	e = u.find_first_of('-', s);
	if (!grab_8hex(u, s, e, uuid_.time_low)) {
		throw invalid_hexfield(1, "Wrong field-1");
	}
	s = e + 1;

	// Grab the time-mid part
	e = u.find_first_of('-', s);
	if (!grab_4hex(u, s, e, uuid_.time_mid)) {
		throw invalid_hexfield(2, "Wrong field-2");
	}
	s = e + 1;

	// Grab the time-high-and-version part
	e = u.find_first_of('-', s);
	if (!grab_4hex(u, s, e, uuid_.time_hi_and_version)) {
		throw invalid_hexfield(3, "Wrong field-3");
	}
	s = e + 1;

	// Grab clock-seq-and-reserverd clock-seq-low part
	e = u.find('-', s);
	if (!grab_4hex(u, s, e, uuid_.clock_seq)) {
		throw invalid_hexfield(4, "Wrong field-4");
	}
	s = e + 1;

	// Grab the node part
	e = u.size();
	if (!grab_12hex(u, s, e, uuid_.node)) {
		throw invalid_hexfield(5, "Wrong field-5");
	}

	check_uuid();
}

void UUID::clear(void)
{
	memset(&uuid_, 0, sizeof(struct uuid));
}

Net::UUID & UUID::operator =(const UUID & rhs)
{
	memcpy(&uuid_, &rhs.uuid_, sizeof(struct uuid));

	return *this;
}

bool UUID::operator ==(const UUID & rhs)
{
	if (!memcmp(&uuid_, &rhs.uuid_, sizeof(struct uuid))) {
		return true;
	}

	return false;
}

UUID::operator std::string(void)
{
	std::stringstream s;

	s << std::hex << std::setw(8) << std::setfill('0')
	  << uuid_.time_low;
	s << "-";
	s << std::hex << std::setw(4) << std::setfill('0')
	  << uuid_.time_mid;
	s << "-";
	s << std::hex << std::setw(4) << std::setfill('0')
	  << uuid_.time_hi_and_version;
	s << "-";
	s << std::hex << std::setw(4) << std::setfill('0')
	  << uuid_.clock_seq;
	s << "-";

	int i;
	for (i = 0; i < 6; i++) {
		s << std::hex << std::setw(2) << std::setfill('0')
		  << static_cast<int>(uuid_.node[i]);
	}

	return s.str();
}

std::ostream & operator <<(std::ostream & stream,
			   UUID           obj)
{
	return stream;
}

std::istream & operator >>(std::istream & stream,
			   UUID &         obj)
{
	return stream;
}

void UUID::format_uuid_tb(void)
{
	throw generic_problem("XXX TO BE IMPLEMENTED");
}

void UUID::format_uuid_nb(void)
{
	throw generic_problem("XXX TO BE IMPLEMENTED");
}

void UUID::format_uuid_random(void)
{
	uuid_.time_low             = myrand(0, 0xFFFFFFFF);
	uuid_.time_mid             = (uint16_t) myrand(0, 0xFFFF);
	uuid_.time_hi_and_version  = (uint16_t) myrand(0, 0xFFFF);
	uuid_.time_hi_and_version &= 0x0FFF;
	uuid_.time_hi_and_version |= (IETF_RFC4122_RANDOM << 12);
	uuid_.clock_seq            = (uint16_t) myrand(0, 0xFFFF);
	uuid_.clock_seq           &= 0x3FFF;
	uuid_.clock_seq           |= (0x2 << 14); // variant

	size_t i;
	for (i = 0; i < 6; i++) {
		uuid_.node[i] = (uint8_t) myrand(0, 0xFF);
	}



	// From RFC4122
	// This bit is the unicast/multicast bit, which will never be set in
	// IEEE 802 addresses obtained from network cards.  Hence, there can
	// never be a conflict between UUIDs generated by machines with and
	// without network cards.
	uuid_.node[0] |= (0x1 << 7);
}

void UUID::scramble_sha1(void)
{
	throw generic_problem("XXX TO BE IMPLEMENTED");
}

void UUID::scramble_md5(void)
{
	throw generic_problem("XXX TO BE IMPLEMENTED");
}


void UUID::check_uuid(void)
{
	version_t ver;
	uint8_t   variant;

	ver     = (version_t) (uuid_.time_hi_and_version >> 12);
	variant =  (uuid_.clock_seq & 0xC000) >> 14;

	if (variant != 2) {
		throw formatting_problem(ver,
					 variant,
					 "UUID has a fake variant");
	}

	switch (ver) {
		case IETF_RFC4122_TIMEBASED: {
			// XXX-FIXME
		}
			break;
		case DCE_SECURITY_POSIX_UID: {
			// XXX-FIXME
		}
			break;
		case IETF_RFC4122_NAMEBASED_MD5: {
			// XXX-FIXME
		}
			break;
		case IETF_RFC4122_NAMEBASED_SHA1: {
			// XXX-FIXME
		}
			break;
		case IETF_RFC4122_RANDOM: {
			if (!(uuid_.node[0] >> 7)) {
				throw formatting_problem(ver,
							 -1,
							 "UUID has a "
							 "non-multicast "
							 "node-id");
			}
		}
			break;
		default: {
			throw formatting_problem(ver,
						 3,
						 "UUID has a fake version");
		}
			break;
	}
}
