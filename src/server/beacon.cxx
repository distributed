//
// Copyright (C) 2008 Francesco Salvestrini
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
//

#include "config.h"

#include <unistd.h>
#include <cstdlib>
#include <string>
#include <vector>
#include <iostream>

#include "getopt.h"

#include "libs/misc/debug.h"
#include "libs/misc/constant.h"
#include "libs/misc/environment.h"
#include "libs/misc/utility.h"
#include "libs/conf/configuration.h"
#include "libs/vm/vm.h"
#ifdef HAVE_LUA
#include "libs/vm/lua.h"
#endif
#include "libs/net/uuid.h"

#define PROGRAM_NAME "beacon"

void version(void)
{
	std::cout
		<< PROGRAM_NAME << " (" << PACKAGE_NAME  << ") " << PACKAGE_VERSION <<               std::endl
		<<                                                                                   std::endl
		<< "Copyright (C) 2008 Francesco Salvestrini" <<                                     std::endl
		<<                                                                                   std::endl
		<< "This is free software.  You may redistribute copies of it under the terms of" << std::endl
		<< "the GNU General Public License <http://www.gnu.org/licenses/gpl.html>." <<       std::endl
		<< "There is NO WARRANTY, to the extent permitted by law." <<                        std::endl;

}

#define DEFAULT_MAX_MEM  16 * KB
#define DEFAULT_MAX_JOBS 8

void help(void)
{
	std::cout
		<< "Usage: " << PROGRAM_NAME << " [OPTION]... " <<                                                  std::endl
		<<                                                                                                  std::endl
		<< "Options: " <<                                                                                   std::endl
		<< "  -m, --max-mem     maximum usable memory in KB (default " << DEFAULT_MAX_MEM / KB << " KB)" << std::endl
		<< "  -j, --max-jobs    maximum number of parallel jobs (default " << DEFAULT_MAX_JOBS << ")" <<    std::endl
		<< "  -d, --debug       enable debugging traces" <<                                                 std::endl
		<< "  -h, --help        print this help, then exit" <<                                              std::endl
		<< "  -V, --version     print version number, then exit" <<                                         std::endl
		<<                                                                                                  std::endl
		<< "Report bugs to <" << PACKAGE_BUGREPORT << ">" <<                                                std::endl;
}

void hint(const std::string & message)
{
	BUG_ON(message.size() == 0);

	std::cout
		<< message <<                                                 std::endl
		<< "Try `" << PROGRAM_NAME << " -h' for more information." << std::endl;
}

int main(int argc, char * argv[])
{
	TR_CONFIG_LVL(TR_LVL_DEFAULT);
	TR_CONFIG_PFX(PROGRAM_NAME);

	try {
		std::string conffile = "";

		int  max_mem         = DEFAULT_MAX_MEM;
		bool max_mem_set     = false;
		int  max_jobs        = DEFAULT_MAX_JOBS;
		bool max_jobs_set    = false;

		int c;
		//int digit_optind = 0;
		while (1) {
			//int this_option_optind = optind ? optind : 1;
			int option_index       = 0;

			static struct option long_options[] = {
				{ "config",   1, 0, 'c' },
				{ "max-mem",  1, 0, 'm' },
				{ "max-jobs", 1, 0, 'j' },
				{ "debug",    0, 0, 'd' },
				{ "verbose",  0, 0, 'v' },
				{ "version",  0, 0, 'V' },
				{ "help",     0, 0, 'h' },
				{ 0,          0, 0, 0   }
			};

			c = getopt_long(argc, argv, "c:m:j:dvVh",
					long_options, &option_index);
			if (c == -1) {
				break;
			}

			switch (c) {
				case 'c':
					conffile = optarg;
					break;
				case 'm':
					//
					// XXX FIXME:
					//     The current solution is ugly.
					//     Add modifiers (KB, MB, GB) to
					//     this option in order to use
					//     different memory granularity.
					//
					max_mem = atoi(optarg);
					if (max_mem <= 0) {
						hint("Wrong max memory value");
						return 1;
					}
					max_mem     = max_mem * 1024;
					max_mem_set = true;
					break;
				case 'j':
					max_jobs = atoi(optarg);
					if (max_jobs <= 0) {
						hint("Wrong max jobs value");
						return 1;
					}
					max_jobs_set = true;
					break;
				case 'd':
					TR_CONFIG_LVL(TR_LVL_DEBUG);
					break;
				case 'v':
					TR_CONFIG_LVL(TR_LVL_VERBOSE);
					break;
				case 'V':
					version();
					return 0;
				case 'h':
					help();
					return 0;
				case '?':
					hint("Unrecognized option");
					return 1;

				default:
					BUG();
					return 1;
			}
		}

		// Options related checks
		BUG_ON(max_mem <= 0);
		BUG_ON(max_jobs <= 0);

		// Build configuration file path
		if (conffile.size() == 0) {
			std::string homedir = Environment::get("HOME");
			std::string confdir =
				homedir          +
				std::string("/") +
				std::string(".") +
				std::string(PACKAGE_TARNAME);
			conffile =
				confdir          +
				std::string("/") +
				std::string(PROGRAM_NAME);
		} else {
			TR_DBG("Configuration file overridden\n");
		}

		BUG_ON(conffile.size() == 0);

		TR_DBG("Initial (configuration file) values:\n");
		TR_DBG("  Max mem:  '%d'\n", max_mem);
		TR_DBG("  Max jobs: '%d'\n", max_jobs);

		// Read configuration file (if available)
		try {
			TR_DBG("Reading configuration file from '%s'\n",
			       conffile.c_str());

			Configuration::File config;
			std::ifstream       instream(conffile.c_str());

			instream >> config;

			int conf_max_mem;
			if (config.get<int>(conf_max_mem, "max-mem")) {
				TR_DBG("Found 'max-mem' key, value '%d'\n",
				       conf_max_mem);

				// Check gathered configuration
				if (conf_max_mem < 0) {
					TR_ERR("Wrong max-mem value in "
					       "configuration file");
					return 1;
				}

				if (!max_mem_set) {
					TR_DBG("Updating 'max-mem' key\n");

					// Configuration value not specified
					// in command line ...
					max_mem = conf_max_mem;
				}
			}

			int conf_max_jobs;
			if (config.get<int>(conf_max_jobs, "max-jobs")) {
				TR_DBG("Found 'max-jobs' key, value '%d'\n",
				       conf_max_mem);

				// Check gathered configuration
				if (conf_max_jobs < 0) {
					TR_ERR("Wrong max-jobs value in "
					       "configuration file");
					return 1;
				}

				if (!max_jobs_set) {
					TR_DBG("Updating 'max-jobs' key\n");

					// Configuration value not specified
					// in command line ...
					max_jobs = conf_max_jobs;
				}
			}
		} catch (std::exception & e) {
			TR_ERR("%s\n", e.what());
		} catch (...) {
			BUG();
		}

		// Options related checks
		BUG_ON(max_mem <= 0);
		BUG_ON(max_jobs <= 0);

		TR_DBG("Final (configuration file) values:\n");
		TR_DBG("  Max mem:  '%d'\n", max_mem);
		TR_DBG("  Max jobs: '%d'\n", max_jobs);

		size_t mem_for_vm = max_mem / max_jobs;

		// Dump (acquired and derived) infos
		TR_DBG("Max mem:            '%d'\n", max_mem);
		TR_DBG("Max jobs            '%d'\n", max_jobs);
		TR_DBG("Mem for VM          '%d'\n", mem_for_vm);
		TR_DBG("Configuration file: '%s'\n", conffile.c_str());

		if (mem_for_vm < MIN_MEM_FOR_VM) {
			TR_CRT("Memory for single VM is too small "
			       "(%d byte%s each, %d minimum)\n",
			       mem_for_vm, PLURAL(mem_for_vm), MIN_MEM_FOR_VM);
			return 1;
		}

		TR_DBG("Building %d VM%s\n", max_jobs, PLURAL(max_jobs));

		std::vector<VM::VM *> vms;
		vms.resize(max_jobs);

		std::vector<VM::VM *>::iterator iter;
		for (iter = vms.begin(); iter != vms.end(); iter++) {
			try {
#ifdef HAVE_LUA
				(*iter) = new VM::Lua(mem_for_vm);
#endif
			} catch (...) {
				TR_CRT("Cannot create VM\n");
				return 1;
			}
			TR_DBG("VM %p initialized\n", (*iter));
		}

		size_t slack = max_mem - max_jobs * mem_for_vm;
		TR_DBG("Memory slack is %d byte%s\n", slack, PLURAL(slack));

		// Do the main loop here
		try {
			std::string t0("7d444840-9dc0-11d1-b245-5ffdce74fad2");
			std::string t1("e902893a-9d22-3c7e-a7b8-d6e313b71d9f");
			std::string t2;

			std::cout << t1 << std::endl;

			Net::UUID uuid0(t0);

			t2 = std::string(uuid0);

			Net::UUID uuid1(t1);

			t2 = std::string(uuid1);

			std::cout << t2 << std::endl;

			BUG_ON(t1 != t2);

			for (size_t i = 0; i < 5; i++) {
				Net::UUID uuidRand(Net::UUID::IETF_RFC4122_RANDOM);

				t2 = std::string(uuidRand);

				std::cout
					<< "--- RANDOM #" << i
					<< ":" << t2 << std::endl;
			}

			Net::UUID uuidTB(Net::UUID::UNKNOWN/*IETF_RFC4122_TIMEBASED*/);

		} catch (Net::UUID::invalid_version & e) {
			TR_ERR("Got Net::UUID::invalid_version "
			       "exception 'Version 0x%x %s'\n",
			       e.version,
			       e.what());
			throw e;
		} catch (Net::UUID::formatting_problem & e) {
			TR_ERR("Got Net::UUID::formatting_problem "
			       "exception 'Version 0x%x %s %d'\n",
			       e.version,
			       e.what(),
			       e.field);
			throw e;
		} catch (Net::UUID::invalid_hexfield & e) {
			TR_ERR("Got Net::UUID::invalid_hexfield "
			       "exception '%s'\n",
			       e.what());
			throw e;
		} catch (Net::UUID::generic_problem & e) {
			TR_ERR("Got Net::UUID::generic_problem "
			       "exception '%s'\n",
			       e.what());
			throw e;
		} catch (std::exception & e) {
			TR_ERR("Got exception '%s'\n", e.what());
			throw e;
		}

		MISSING_CODE();
	} catch (...) {
		BUG();
	}

	return 0;
}
